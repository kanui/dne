// Copyright 2013 Zocprint Serviços Gráficos Ltda. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package schema

import (
	"io"
	"os"
	"path"
)

// LOG_UNID_OPER.TXT - Unidade Operacional dos Correios
// São agências próprias ou terceirizadas, centros de distribuição, etc. O campo
// LOG_NU está sem conteúdo para as localidades não codificadas (LOG_IN_SIT=0),
// devendo set utilizado o campo UOP_ENDEREÇO para endereçamento.
type UnidadeOperacional struct {
	UF            *UF           `json:"-"` //     UFE_SG        sigla da UF                      CHAR(2)
	Localidade    *Localidade   //     LOC_NU        chave da localidade              NUMBER(8)
	Bairro        *Bairro       //     BAI_NO        chave do bairro                  NUMBER(8)
	Logradouro    *Logradouro   `json:",omitempty"` //     LOG_NU        chave do logradouro (opcional)   NUMBER(8)
	Nome          string        //     UOP_NO        nome                             VARCHAR(72)
	Endereço      string        //     UOP_ENDERECO  endereço                         VARCHAR(100)
	CEP           uint32        //     CEP           CEP                              CHAR(8)
	CaixaPostal   bool          //     UOP_IN_CP     indicador de caixa postal (S/N)  CHAR(1)
	Abreviatura   string        `json:",omitempty"` //     UOP_NO_ABREV  abreviatura do nome (opcional)   VARCHAR(36)
	CaixasPostais []CaixaPostal `json:",omitempty"`
}

// LOG_FAIXA_UOP.TXT - Faixa de Caixa Postal - UOP
type CaixaPostal struct {
	Inicial uint32 // PK  FNC_INICIAL  número inicial  NUMBER(8)
	Final   uint32 //     FNC_FINAL    número final    NUMBER(8)
}

type UnidadeOperacionalMap map[uint32]*UnidadeOperacional

var (
	UnidadesOperacionais  = make(UnidadeOperacionalMap)
	CEPUnidadeOperacional = make(UnidadeOperacionalMap)
)

const (
	UNIDADE_OPERACIONAL_FILE_NAME = "LOG_UNID_OPER.TXT"
	CAIXA_POSTAL_FILE_NAME        = "LOG_FAIXA_UOP.TXT"
)

func NewUnidadeOperacionalFromStrings(strs []string, filename string) (uint32, *UnidadeOperacional, error) {
	id, err := stringToUint32(strs[0])
	if err != nil {
		return 0, nil, err
	}
	if _, ok := UFs[strs[1]]; !ok {
		return id, nil, MissingEntityError("UF", strs[1], filename)
	}
	idloc, err := stringToUint32(strs[2])
	if err != nil {
		return id, nil, err
	}
	loc, ok := Localidades[idloc]
	if !ok {
		return id, nil, MissingEntityError("Localidade", strs[2], filename)
	}
	idb, err := stringToUint32(strs[3])
	if err != nil {
		return id, nil, err
	}
	bai, ok := Bairros[idb]
	if !ok {
		return id, nil, MissingEntityError("Bairros", strs[3], filename)
	}
	idlog, err := optionalStringToUint32(strs[4], 0)
	if err != nil {
		return id, nil, err
	}
	logr, ok := Logradouros[idlog]
	if idlog > 0 && !ok {
		return id, nil, MissingEntityError("Logradouro", strs[4], filename)
	}
	cep, err := stringToUint32(strs[7])
	if err != nil {
		return id, nil, err
	}
	cxp := false
	if strs[8] == "S" {
		cxp = true
	}
	return id, &UnidadeOperacional{
		UF:          UFs[strs[1]],
		Localidade:  loc,
		Bairro:      bai,
		Logradouro:  logr,
		Nome:        strs[5],
		Endereço:    strs[6],
		CEP:         uint32(cep),
		CaixaPostal: cxp,
		Abreviatura: strs[9],
	}, nil
}

func LoadUnidadesOperacionais(baseDir string) error {
	fname := path.Join(baseDir, UNIDADE_OPERACIONAL_FILE_NAME)
	file, err := os.Open(fname)
	if err != nil {
		return err
	}
	defer file.Close()
	reader, err := startReader(file)
	if err != nil {
		return err
	}
	for {
		cols, err := reader.Read()
		if err != nil {
			if err == io.EOF {
				break
			}
			return err
		}
		id, uo, err := NewUnidadeOperacionalFromStrings(cols, UNIDADE_OPERACIONAL_FILE_NAME)
		if err != nil {
			return err
		}
		UnidadesOperacionais[id] = uo
		CEPUnidadeOperacional[uo.CEP] = uo
	}
	return loadCaixasPostais(baseDir)
}

func loadCaixasPostais(baseDir string) error {
	fname := path.Join(baseDir, CAIXA_POSTAL_FILE_NAME)
	file, err := os.Open(fname)
	if err != nil {
		return err
	}
	defer file.Close()
	reader, err := startReader(file)
	if err != nil {
		return err
	}
	for {
		cols, err := reader.Read()
		if err != nil {
			if err == io.EOF {
				break
			}
			return err
		}
		id, err := stringToUint32(cols[0])
		if err != nil {
			return err
		}
		if _, ok := UnidadesOperacionais[id]; !ok {
			return MissingEntityError("UnidadeOperacional", cols[0], CAIXA_POSTAL_FILE_NAME)
		}
		nini, err := stringToUint32(cols[1])
		if err != nil {
			return err
		}
		nfin, err := stringToUint32(cols[2])
		if err != nil {
			return err
		}
		UnidadesOperacionais[id].CaixasPostais = append(UnidadesOperacionais[id].CaixasPostais, CaixaPostal{nini, nfin})
	}
	return nil
}
