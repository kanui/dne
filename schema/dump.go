package schema

import (
	"fmt"
	"os"
)

func DumpCEPs(filename string) {
	f, err := os.OpenFile(filename, os.O_CREATE|os.O_WRONLY, os.ModePerm)
	if err != nil {
		panic(err)
	}
	defer f.Close()

	for cep, _ := range CEPLocalidade {
		f.WriteString(fmt.Sprintf("%08d\n", cep))
	}
	for cep, _ := range CEPLogradouro {
		f.WriteString(fmt.Sprintf("%08d\n", cep))
	}
	for cep, _ := range CEPGrandeUsuário {
		f.WriteString(fmt.Sprintf("%08d\n", cep))
	}
	for cep, _ := range CEPUnidadeOperacional {
		f.WriteString(fmt.Sprintf("%08d\n", cep))
	}
	for cep, _ := range CEPCaixaPostalComunitária {
		f.WriteString(fmt.Sprintf("%08d\n", cep))
	}
}
