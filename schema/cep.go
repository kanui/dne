// Copyright 2013 Zocprint Serviços Gráficos Ltda. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package schema

type FaixaCEP struct {
	Inicial uint32 // CEP inicial   CHAR(8)
	Final   uint32 // CEP final     CHAR(8)
}

func NewFaixaCEPFromStrings(strs []string) (string, *FaixaCEP, error) {
	id := strs[0]
	cepi, err := stringToUint32(strs[1])
	if err != nil {
		return id, nil, err
	}
	cepf, err := stringToUint32(strs[2])
	if err != nil {
		return id, nil, err
	}
	return id, &FaixaCEP{
		Inicial: cepi,
		Final:   cepf,
	}, nil
}

type CEPResponse struct {
	Localidade             *Localidade             `json:",omitempty"`
	Logradouro             *Logradouro             `json:",omitempty"`
	GrandeUsuário          *GrandeUsuário          `json:",omitempty"`
	UnidadeOperacional     *UnidadeOperacional     `json:",omitempty"`
	CaixaPostalComunitária *CaixaPostalComunitária `json:",omitempty"`
}

func SearchCEP(cep uint32) *CEPResponse {
	if obj, ok := CEPLocalidade[cep]; ok {
		return &CEPResponse{Localidade: obj}
	}
	if obj, ok := CEPLogradouro[cep]; ok {
		return &CEPResponse{Logradouro: obj}
	}
	if obj, ok := CEPGrandeUsuário[cep]; ok {
		return &CEPResponse{GrandeUsuário: obj}
	}
	if obj, ok := CEPUnidadeOperacional[cep]; ok {
		return &CEPResponse{UnidadeOperacional: obj}
	}
	if obj, ok := CEPCaixaPostalComunitária[cep]; ok {
		return &CEPResponse{CaixaPostalComunitária: obj}
	}
	return nil
}
