// Copyright 2013 Zocprint Serviços Gráficos Ltda. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	"github.com/yvasiyarov/gorelic"
)

var (
	newRelicAgent gorelic.Agent
)

func setupNewRelic() {
	if newRelicLicense == "" {
		return
	}

	newRelicAgent := gorelic.NewAgent()

	newRelicAgent.NewrelicLicense = newRelicLicense
	newRelicAgent.NewrelicName = newRelicAppName
	newRelicAgent.Verbose = (bool)(verbose)
	newRelicAgent.GCPollInterval = 60

	newRelicAgent.Run()
}
